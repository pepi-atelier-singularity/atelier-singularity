<div align="center">
<h1><br>[Formationn Singularity](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity)<br> <img src="https://www.inrae.fr/themes/custom/inrae_socle/logo.svg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Séminnaire CATI-PEPI-Pépinière 2022 Sète](https://nextcloud.inrae.fr/s/xDgqmFZokLJq8Rd)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://www6.inrae.fr/var/internet6_national_dipso/storage/images/configuration-graphique/haut/dipso2/34393-3-fre-FR/DipSO_inra_logo.png"></h1>

<h2>Singularity&nbsp;<img src="./img/singularity.png" width="50" height="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mesocentre&nbsp;<img src="./img/Logo_MESOLR.jpeg" width="100" height="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CINES&nbsp;<img src="./img/logo-site211.png" width="80" height="50"></h2>
</div>

---
Bienvenue à l'atelier Singularity, notre but est de vous donner un aperçu de ce qu'il est possible de faire avec les images [Singularity](https://singularityhub.github.io/singularityhub-docs/docs/introduction).

Quick start:
https://sylabs.io/guides/3.9/user-guide/quick_start.html
- Vous allez découvrir comment créer une image, la lancer simplement et aller un peu plus loin dans son utilisation.
Suivant votre avancée dans l'utilisation de Singularity, vous pourrez naviguer sur les différents TPs proposés.
* Voici un tutoriel officiel pour démarrer avec singularity [sylabs.io user-guide](https://sylabs.io/guides/latest/user-guide/quick_start.html)
* Documentation Singularity [singularity hub doc](https://singularityhub.github.io/singularityhub-docs/)

* Lien vers les dépos d'images Singularity des packages conda [galaxyproject](https://depot.galaxyproject.org/singularity)

* Lien vers exemples de recettes et d'images Singularity sur la forgemia (utilisation CI/CD) [https://forgemia.inra.fr/gafl/singularity](https://forgemia.inra.fr/gafl/singularity)

La présentation d'aujourd'hui est disponible ici :[Présentation Singularity, journée CATI/PEPI](https://nextcloud.inrae.fr/s/2FajsHwHLFkHjCD)

## Pré-requis :
Local:
Un Linux ou une vm Linux basée sur Debian.

Remote:
Une machine virtuelle Linux a été créée sur le SI INRAE de Montpellier accessible via ssh:
```bash
ssh <mylogin>@195.221.108.97
```
Il vous faut donc un client SSH capable de s'y connecter.
Sous Windows, vous pouvez utiliser l'utilitaire:
 [MobaXTerm](https://mobaxterm.mobatek.net/download.html) ou [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)

Sous Linux et Mac, SSH est disponible sur votre OS à l'aide du terminal.

 Pour le TP9 exemple de container avec une application X11, vous pouvez utiliser [x2go client](https://wiki.x2go.org/doku.php/doc:installation:x2goclient) ou  [MobaXTerm](https://mobaxterm.mobatek.net/download.html) 
  
Tous les fichiers mentionnés dans les TP sont disponibles sur le Gitlab de la forgeMIA: [atelier-singularity](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity)
Vous pouvez également en faire un clone sur votre espace de travail (conseillé):
```bash
git clone https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity.git
```
### Pour une brève introduction: [Singularity_introduction.adoc](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/blob/master/Singularity_introduction.adoc)

### La présentation en pdf : [Singularity.pdf]()

  
Singularity on the forgemia: https://forgemia.inra.fr/singularity
GAFL Avignon: https://forgemia.inra.fr/gafl/singularity
Inter-Cati-omics: https://forgemia.inra.fr/inter_cati_omics


## 1) [TP1](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP1_install) Installation de Singularity

Exemple de Singularity version 3.9.2 sur un Debian 11
Vous pouvez retrouver un TP qui explique comment __installer Singularity__ à partir d'une __machine linux__ ou bien d'une __VM__.
Le script shell installe Singularity en 3 étapes:

- 1) Installation des dépendences

- 2) Go language

- 3) compilation et installation de Singularity

  
## 2) [TP2](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP2_first_containers) Création et utilisation d'une première image Singularity
TP2_R_base, Voici un tutoriel pour apprendre à __créer un conntainer en intéractif et à base de "recettes" et lancer des images Singularity__.
Ce tp permet:
- La contruction en mode intéartif
- La création d'un fichier de recette (exemple R)
- La construction d'une image à partir d'un fichier recette
- L'éxécution d'image Singularity

  
## 3) [TP3](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP3_CICD) Utilisation de CI/CD sur gitlab (continus integration et continus deployment)
Permet la création automatique d'images sans passer par une machine Linux:
**! Attention** Sur la forge mia, la taille maximale d'une image est de 4Go.

TP sur le __CI/CD__ pour les utilisateurs et administrateurs.
Création d'un GitLab CI/CD pipeline configuration: ".gilab-ci.yml"
Ce tp permet de:
1) lancer le build automatiquement 
- Un exemple de recette Singularity (R base)
2) déployer l'image sur le gitlab de la forge avec OCI (Open container initiative)
- Comment obtenir l'image (pull) en utilisant ORAS: OCI Registry As Storage (https://oras.land/)
  

## 4) [TP4](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP4_conda) Utilisation du système de packaging Conda dans un container Singularity

[Conda](https://docs.conda.io/en/latest/)
[Conda Getting started](https://docs.conda.io/projects/conda/en/latest/user-guide/getting-started.html)
L'utilisation de conda encapsulé dans un container permet une installation simplifiée de logiciels versionnés et leur dépendances.

Ce tp permet:
- L'installation de [miniconda](https://docs.conda.io/en/latest/miniconda.html) embarqué dans un container.
- L'installation de packages conda dans un container


## 5) [TP5](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP5_mpi) Création d'image pour executer du code MPI

Permet d'encapsulé un binaire MPI dans un container.

Et d'éxécuter le binaire dans un environement HPC

!!! Il faut que le mpi à l'intérieur du container soit compabile avec celui de l'hôte.

Ce tp permet:

- L'installation des libraries OpnMPI

- La Compilation de code MPI

- L'éxecution du code MPI

- L'utilisation dans un environement HPC (avec slurm)

  

## 6) [TP6](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP6_prebuilt_image) Réutilisation d'image de bases préconstruites.

Utilisation d'une image pré-construite comme base d'un nouveau container.

Permet ainsi la réduction du temps de build du nouveau container.

- exemple utilisation d'une image pre-built contenant miniconda et "r-essentials.3.6"

- A partir de cette image nous avons juste à ajouter des packages R

__! Attention__ Il faut que l'image pré-construire suive les bonnes pratiques (versions, documentations,...)

build time sans prebuilt: 8m30s

build time avec prebuilt: 1m05s

  

## 7) [TP7](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP7_multi_stage) Utilisation du Build multi-Stage.

Permet de réduire la taille de l'image finale.

Il y a 2 exemples:

Exemple 1) "Hello World" en Go

- stage 1 installation de Go lang et compilation du code source

- stage 2 construction de l'image finale avec seulement le systeme de base et le programme compilé.

Sans multi-stage image finale pèse 122Mo

Avec multi-stage image finale pèse 3.6Mo

  

Exemple 2) Utilisation de conda Avec R et combine l'utilisation d'unne image pré-construite

- stage 1 installation de Miniconda et installation de r-essentials dans un connda env

- stage 2 onn ne garde que l'environement minimal conda R dans l'image finale.

Sans multi-stage image finale pèse 1.1Go

Avec multi-stage image finale pèse 298Mo

  

  

## 8) [TP8](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP8_microservice) Création d'image pour des Services

Exemple de serveur Apache.

Ce TP vous permettra de contruire une image contenant apache2 serveur.

Et de l'utiliser en daemon avec singularity instance.

  

## 9) [TP9](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP9_x11) Construction et Utilisation d'image avec interface graphique

Ce TP vous permettra de construire une image avec unne application graphique (GUI) et de l'exécuter.

Avec pour exemple: Rstudio

Utiliser ssh -X ou x2go client avec un xterm pour l'utiliser

  

## 10) [TP10](https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/-/tree/master/TP10_conversions) Conversion de recettes Dockers en recettes Singularity

Conversions entre dockerfile et singularity définition file

Exemple avec un dockerfile

>[!WARNING]__! Attention__  Prefer to use recipes conversions reather images conversions
