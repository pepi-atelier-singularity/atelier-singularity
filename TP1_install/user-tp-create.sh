#! /usr/bin/env bash 
#set -x 
set -e

### PARAMETRES
read -rp "Combien d'user souhaitez-vous créer ? " nb_users
read -rp "Quel préfixe ? " prefixe_users

### SCRIPTS
for i in $(seq 1 1 ${nb_users})
do
        passwd_users=$(openssl rand -base64 12)
        useradd ${prefixe_users}${i} -G sudo --password $(mkpasswd --hash=SHA-512 ${passwd_users}) --create-home --home-dir /home/${prefixe_users}${i} --shell /bin/bash
        echo "${prefixe_users}${i};${passwd_users}"
        echo ". /usr/local/etc/bash_completion.d/singularity" >> /home/"${prefixe_users}${i}"/.bashrc
done

unset prefixe_users nb_users passwd_users i
