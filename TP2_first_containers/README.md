# Singularity container: Build and execution of containers
## use case: R_base Version: 3.6.3
## We will build 3 containers using the [singularity pdf](https://nextcloud.inrae.fr/s/2FajsHwHLFkHjCD ) as a guide (on slide 31 and following)
-  Without definition file
-  With definition file (recipe)
-  R using optimised BLAS

## 1) Without definition file: interactive built

a) using a sandbox from a docker debian
```bash
sudo singularity build --sandbox R_base.sandbox docker://ubuntu:20.04
```

b) we run it interactive mode in r/w
```bash
sudo singularity shell --writable R_base.sandbox
```

c) we will install packages using apt
   - as we will do on a regulation Linux box
```bash
# update tree
apt update && apt -y upgrade
#installation of R_base & nano
apt install -y r-base nano
```
 - When we have finish to install the package, we are ready to use R inside the container.
```bash
# we can test R
R
Sys.info()
sessionInfo()
#Now We exit the container
exit
# and execute R inside the container
singularity exec R_base.sandbox R
Sys.info()
sessionInfo()
```

 - We now can convert the sandbox in image singularity format (sif) using readonly fs (squashfs)
```bash
# image convertions
sudo singularity build R_base.sif R_base.sandbox
# Execution of the container like a regular binary
./R_base.sif
# inside the container
Sys.info()
sessionInfo()
```

## 2) Singularity container based on the recipe: Singularity.R.3.6.3.def

- We will replicate all the steps from the interactive build part and make a standalone text file (called def) which, when build, will create a container similar to the first one.

- To build an image from a def file locally, please use the following command:

```bash
sudo singularity build R.3.6.3.sif Singularity.R.3.6.3.def
```

- The container is like a regular application. R is the default program that we defined in the %runscript section of the ".def" file
```bash
./build R.3.6.3.sif
```
- We can also execute a software inside the container =>
```bash
singularity exec R.3.6.3.sif R
n <- 2000 ; X <- matrix(rnorm(n*n),n,n); system.time(svd(X))
sessionInfo()
 
```


## 3) we add intel-mkl inn the sandbox created before (in 1)

- First we run the following command to use our first container
```bash
sudo singularity shell --writable R_base.sandbox
```
- Then we install BLAS with MKL support =>
```bash
apt update

## install BLAS with MKL support
## cf https://software.intel.com/en-us/articles/installing-intel-free-libs-and-python-apt-repo

wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB
apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB

## all products:
#wget https://apt.repos.intel.com/setup/intelproducts.list -O /etc/apt/sources.list.d/intelproducts.list
## just MKL
sh -c 'echo deb https://apt.repos.intel.com/mkl all main > /etc/apt/sources.list.d/intel-mkl.list'
## other (TBB, DAAL, MPI, ...) listed on page

apt update
DEBIAN_FRONTEND=noninteractive apt install -y intel-mkl-64bit-2018.2-046 ## wants 500+mb :-/ installs to 1.8 gb :-/

## update alternatives
DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/libblas.so libblas.so-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50
DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/libblas.so.3 libblas.so.3-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50
DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/liblapack.so liblapack.so-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50
DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/liblapack.so.3 liblapack.so.3-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50

echo "/opt/intel/lib/intel64" > /etc/ld.so.conf.d/mkl.conf
echo "/opt/intel/mkl/lib/intel64" >> /etc/ld.so.conf.d/mkl.conf
ldconfig

echo "MKL_THREADING_LAYER=GNU" >> /etc/environment

```

- We can convert the sandbox in image singularity format (sif) using readonly fs (squashfs)

```bash
# image convertions
sudo singularity build R_base_mkl.sif R_base.sandbox
# use R
singularity exec R_base.sif R
# inside the container
Sys.info()
sessionInfo()
```

- We can also use the recipe __Singularity.R.3.6.3_mkl.def__ to build the containner

```bash
# image build
sudo singularity build R.3.6.3_mkl.sif Singularity.R.3.6.3_mkl.def
# use R
./R.3.6.3_mkl.sif
# inside the container
Sys.info()
sessionInfo()
```

