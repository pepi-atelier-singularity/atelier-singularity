# distribution based on: ubuntu 20.04
Bootstrap:docker
From:ubuntu:20.04

# container for R version 3.6 installed with conda
# Build local:
# sudo singularity build R.3.6.3_mkl.sif Singularity.R.3.6.3_mkl.def

%environment
export LC_ALL=C
export LC_NUMERIC=en_US.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

%labels
COPYRIGHT INRAE 2022
VERSION 1.0
LICENSE MIT
DATE_MODIF MYDATEMODIF

%help
Container with R
Version: 3.6.3
Intallation with miniconda3 v4.7.12
Path: /opt/miniconda

Default runscript: R

%test
    exec /opt/miniconda/bin/R --version


%runscript
    #default runscript: bedops passing all arguments from cli: $@
    exec /opt/miniconda/bin/R "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    DEBIAN_FRONTEND=noninteractive apt upgrade -y
    apt install -y wget bzip2 nano

     #install conda
    cd /opt
    #miniconda3: get miniconda3 version 4.7.12
    wget https://repo.continuum.io/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"
    #add channels
    conda config --add channels conda-forge
    conda config --set channel_priority strict

    conda install -y libcblas=3.9.0=9_mkl liblapack=3.9.0=9_mkl libblas=3.9.0=9_mkl
    conda install -y r-essentials=3.6

    R -e 'install.packages(c("DiceEval","DiceKriging","deepgp","tidyr","dplyr"), repos = "https://cloud.r-project.org")'


    # clean up
    apt autoremove --purge
    apt clean

